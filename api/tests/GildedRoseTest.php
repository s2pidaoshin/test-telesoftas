<?php
namespace App\Tests;

use App\Component\GildedRose\AgedItem;
use App\Component\GildedRose\BackstageItem;
use App\Component\GildedRose\GildedRose;
use App\Component\GildedRose\Item;
use App\Component\GildedRose\LegendaryItem;
use App\Component\GildedRose\StandardItem;
use App\Component\GildedRose\TypedItem;
use PHPUnit\Framework\TestCase;

/**
 * Class GildedRoseTest
 *
 * - All items have a SellIn value which denotes the number of days we have to sell the item
 * - All items have a Quality value which denotes how valuable the item is
 * - At the end of each day our system lowers both values for every it
 *
 * - Once the sell by date has passed, Quality degrades twice as fast
 * - The Quality of an item is never negative
 * - "Aged Brie" actually increases in Quality the older it gets
 * - The Quality of an item is never more than 50
 * - "Sulfuras", being a legendary item, never has to be sold or decreases in Quality
 * - "Backstage passes", like aged brie, increases in Quality as its SellIn value approaches;
 * Quality increases by 2 when there are 10 days or less and by 3 when there are 5 days or less but
 * Quality drops to 0 after the concert
 *
 * @package App\Tests
 */
class GildedRoseTest extends TestCase
{
    /**
     * @test Text test to compare values
     */
    public function testTextResults()
    {
        $items = [
            new Item('+5 Dexterity Vest', 10, 20),
            new Item('Aged Brie', 2, 0),
            new Item('Elixir of the Mongoose', 5, 7),
            new Item('Sulfuras, Hand of Ragnaros', 0, 80),
            new Item('Sulfuras, Hand of Ragnaros', -1, 80),
            new Item('Backstage passes to a TAFKAL80ETC concert', 15, 20),
            new Item('Backstage passes to a TAFKAL80ETC concert', 10, 49),
            new Item('Backstage passes to a TAFKAL80ETC concert', 5, 49),
            // this conjured item does not work properly yet
            new Item('Conjured Mana Cake', 3, 6)
        ];

        $rose = new GildedRose($items);

        $days = 15;

        for ($i = 0; $i < $days; $i++) {
            echo("-------- day $i --------\n");
            echo("name, sellIn, quality\n");
            foreach ($items as $item) {
                echo $item . PHP_EOL;
            }
            echo PHP_EOL;
            $rose->updateQuality();
        }

        // Just for test to be green, main content here is output above
        $this->assertTrue(true);
    }

    /**
     * @test Define item type
     */
    public function testItemType()
    {
        $items = [
            new Item('Backstage passes to a TAFKAL80ETC concert', 15, 0),
            new Item('Sulfuras, Hand of Ragnaros', 2, 20),
            new Item('Aged Brie', 2, 0),
            new Item('foo', 2, 0)
        ];

        /** @var TypedItem[] $type */
        $type = [];
        $rose = new GildedRose($items);
        foreach ($items as $item) {
            $type[] = $rose->defineItemType($item);
        }

        $this->assertTrue($type[0] instanceof BackstageItem);
        $this->assertTrue($type[1] instanceof LegendaryItem);
        $this->assertTrue($type[2] instanceof AgedItem);
        $this->assertTrue($type[3] instanceof StandardItem);
        $this->assertTrue($type[3] instanceof TypedItem);
    }

    function testFoo() {
        $items = array(new Item("foo", 0, 0));
        $gildedRose = new GildedRose($items);
        $gildedRose->updateQuality();
        $this->assertEquals("foo", $items[0]->name);
    }

    /**
     * @test At the end of each day our system lowers item quality
     */
    public function testQualityDecreaseAtTheEndOfADay()
    {
        $days = 5;
        $items = [
            new Item('foo', 50, 50)
        ];
        $rose = new GildedRose($items);

        for ($i = 0; $i < $days; $i++) {
            $rose->updateQuality();
        }

        $this->assertEquals(45, $items[0]->quality);
    }

    /**
     * @test Once the sell by date has passed, Quality degrades twice as fast
     */
    public function testOnceSellDatePassedQualityDecreaseTwice()
    {
        $days = 5;
        $items = [
            new Item('foo', 0, 50)
        ];
        $rose = new GildedRose($items);

        for ($i = 0; $i < $days; $i++) {
            $rose->updateQuality();
        }

        $this->assertEquals(40, $items[0]->quality);
    }

    /**
     * @test The Quality of an item is never negative
     */
    public function testItemQualityIsNeverNegative()
    {
        $days = 5;
        $items = [
            new Item('foo', 50, 2),
            new Item('foo', 2, 6),
        ];
        $rose = new GildedRose($items);

        for ($i = 0; $i < $days; $i++) {
            $rose->updateQuality();
        }

        $this->assertTrue($items[0]->quality >= 0);
        $this->assertTrue($items[1]->quality >= 0);
    }

    /**
     * The Quality of an item is never more than 50
     */
    public function testItemQualityIsNeverMoreThan50()
    {
        $days = 10;
        $items = [
            new Item('Aged Brie', 10, 49),
            new Item('Backstage passes to a TAFKAL80ETC concert', 10, 49),
            new Item('Aged Brie', 10, 60),
        ];
        $rose = new GildedRose($items);

        for ($i = 0; $i < $days; $i++) {
            $rose->updateQuality();
        }

        $this->assertEquals(50, $items[0]->quality);
        $this->assertEquals(50, $items[1]->quality);
        // If item is already with more than 50 condition
        // Currently condition is not covered
        $this->assertEquals(60, $items[2]->quality);
    }

    /**
     * @test "Aged Brie" actually increases in Quality the older it gets
     */
    public function testAgedItemIncreaseInQualityOlderItGets()
    {
        $days = 5;
        $items = [
            new Item('Aged Brie', 5, 0)
        ];
        $rose = new GildedRose($items);

        for ($i = 0; $i < $days; $i++) {
            $rose->updateQuality();
        }

        $this->assertEquals(5, $items[0]->quality);
    }

    /**
     * "Sulfuras", being a legendary item, never has to be sold or decreases in Quality
     */
    public function testLegendaryItemNeverBeSoldOrDecreaseInQuality()
    {
        $days = 5;
        $items = [
            new Item('Sulfuras, Hand of Ragnaros', 10, 10),
            new Item('Sulfuras, Hand of Ragnaros', 2, 20),
            new Item('Sulfuras, Hand of Ragnaros', -1, 60),
        ];
        $rose = new GildedRose($items);

        for ($i = 0; $i < $days; $i++) {
            $rose->updateQuality();
        }

        $this->assertEquals(10, $items[0]->quality);
        $this->assertEquals(10, $items[0]->sell_in);
        $this->assertEquals(20, $items[1]->quality);
        $this->assertEquals(2, $items[1]->sell_in);

        // Just for clarification, an item can never have its Quality increase above 50, however "Sulfuras" is a
        // legendary item and as such its Quality is 80 and it never alters.

        // Current conditions do not cover above case
        $this->assertEquals(60, $items[2]->quality);
        // If status is already sold, should it update an item to sold 0? Or not sold 1?
        // Not covered in current conditions
        $this->assertEquals(-1, $items[2]->sell_in);
    }

    /**
     * "Backstage passes", like aged brie, increases in Quality as its SellIn value approaches;
     * - Quality increases by 2 when there are 10 days or less
     * - Quality increases by 3 when there are 5 days or less
     * - but Quality drops to 0 after the concert
     */
    public function testBackstageItemQualityIncrease()
    {
        $days = 10;
        $items = [
            new Item('Backstage passes to a TAFKAL80ETC concert', 15, 0),
            new Item('Backstage passes to a TAFKAL80ETC concert', 10, 0),
            new Item('Backstage passes to a TAFKAL80ETC concert', 5, 5),
            new Item('Backstage passes to a TAFKAL80ETC concert', 15, 20),
            new Item('Backstage passes to a TAFKAL80ETC concert', 10, 49),
            new Item('Backstage passes to a TAFKAL80ETC concert', 5, 49),
        ];

        $rose = new GildedRose($items);

        for ($i = 0; $i < $days; $i++) {
            $rose->updateQuality();
        }

        $this->assertEquals(15, $items[0]->quality);
        $this->assertEquals(25, $items[1]->quality);
        $this->assertEquals(0, $items[2]->quality);
        $this->assertEquals(35, $items[3]->quality);
        $this->assertEquals(50, $items[4]->quality);
        $this->assertEquals(0, $items[5]->quality);
    }

    /**
     * @test "Conjured" items degrade in Quality twice as fast as normal items
     */
    public function testConjuredItemDecreaseInQualityTwiceAsFastAsNormalItem()
    {
        $days = 5;
        $items = [
            new Item('Conjured Mana Cake', 50, 50), // for sell in over 0
            new Item('Conjured Mana Cake', 0, 50) // for sold out
        ];
        $rose = new GildedRose($items);

        for ($i = 0; $i < $days; $i++) {
            $rose->updateQuality();
        }

        $this->assertEquals(40, $items[0]->quality);
        $this->assertEquals(30, $items[1]->quality);
    }
}
