<?php

namespace App\Component\GildedRose;

/**
 * Class AgedItem
 * @package App\Component\GildedRose
 */
class AgedItem extends TypedItem
{
    /**
     * @return mixed|void
     */
    public function update()
    {
        $this->increaseQuality();
        $this->decreaseSellIn();

        if ($this->isExpired()) {
            $this->increaseQuality();
        }
    }
}
