<?php

namespace App\Component\GildedRose;

/**
 * Class LegendaryItem
 * @package App\Component\GildedRose
 */
class LegendaryItem extends TypedItem
{
    /**
     * @return mixed|void
     */
    public function update()
    {
    }
}
