<?php

namespace App\Component\GildedRose;

/**
 * Class StandardItem
 * @package App\Component\GildedRose
 */
class StandardItem extends TypedItem
{
    /**
     * @return mixed|void
     */
    public function update()
    {
        $this->decreaseQuality();
        $this->decreaseSellIn();

        if ($this->isExpired()) {
            $this->decreaseQuality();
        }
    }
}
