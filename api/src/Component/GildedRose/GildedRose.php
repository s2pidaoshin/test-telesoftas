<?php

namespace App\Component\GildedRose;

/**
 * Class GildedRose
 * @package App\Component\GildedRose
 */
class GildedRose
{
    /**
     * @var Item[]
     */
    private $items;

    /**
     * GildedRose constructor.
     *
     * @param Item[] $items
     */
    function __construct($items)
    {
        $this->items = $items;
    }

    /**
     * Update quality of all items
     */
    function updateQuality()
    {
        foreach ($this->items as $item) {
            $typedItem = $this->defineItemType($item);
            $typedItem->update();
        }
    }

    /**
     * Define what item type is provided
     *
     * @param Item $item
     *
     * @return AgedItem|BackstageItem|LegendaryItem|StandardItem|ConjuredItem
     */
    public function defineItemType(Item $item)
    {
        if (strpos($item->name, TypedItem::TYPE_BACKSTAGE) !== false) {
            return new BackstageItem($item);
        }

        if (strpos($item->name, TypedItem::TYPE_AGED) !== false) {
            return new AgedItem($item);
        }

        if (strpos($item->name, TypedItem::TYPE_LEGENDARY) !== false) {
            return new LegendaryItem($item);
        }

        if (strpos($item->name, TypedItem::TYPE_CONJURED) !== false) {
            return new ConjuredItem($item);
        }

        return new StandardItem($item);
    }
}
