<?php

namespace App\Component\GildedRose;

use App\Component\GildedRose\Interfaces\TypedItemInterface;

/**
 * Class ItemType
 * This class is created because Item class can not be touched because of nasty goblin :)
 *
 * @package App\Component\GildedRose
 */
abstract class TypedItem implements TypedItemInterface
{
    const MIN_QUALITY = 0;
    const MAX_QUALITY = 50;
    const EXPIRED_AT = 0;

    const TYPE_BACKSTAGE = 'Backstage';
    const TYPE_AGED = 'Aged';
    const TYPE_LEGENDARY = 'Sulfuras';
    const TYPE_CONJURED = 'Conjured';
    const TYPE_STANDARD = 'Standard';

    /**
     * @var Item
     */
    private $item;

    /**
     * ItemType constructor.
     *
     * @param Item $item
     */
    public function __construct(Item $item)
    {
        $this->item = $item;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->item->name;
    }

    /**
     * @return int
     */
    public function getQuality(): int
    {
        return $this->item->quality;
    }

    /**
     * @return int
     */
    public function getSellIn(): int
    {
        return $this->item->sell_in;
    }

    /**
     * @param int $value
     */
    public function setQuality(int $value)
    {
        $this->item->quality = (int)$value;
    }

    /**
     * Decrease item quality
     */
    public function decreaseQuality()
    {
        if ($this->getQuality() > self::MIN_QUALITY) {
            $this->item->quality--;
        }
    }

    /**
     * Increase item quality
     */
    public function increaseQuality()
    {
        if ($this->getQuality() < self::MAX_QUALITY) {
            $this->item->quality++;
        }
    }

    /**
     * Decrease item sell in
     */
    public function decreaseSellIn()
    {
        $this->item->sell_in--;
    }

    /**
     * Checks if item sell date is expired
     *
     * @return bool
     */
    public function isExpired()
    {
        return $this->getSellIn() < self::EXPIRED_AT;
    }
}
