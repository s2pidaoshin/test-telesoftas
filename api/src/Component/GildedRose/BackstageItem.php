<?php

namespace App\Component\GildedRose;

/**
 * Class BackstageItem
 * @package App\Component\GildedRose
 */
class BackstageItem extends TypedItem
{
    /**
     * @return mixed|void
     */
    public function update()
    {
        $this->increaseQuality();

        if ($this->getSellIn() <= 10) {
            $this->increaseQuality();
        }

        if ($this->getSellIn() <= 5) {
            $this->increaseQuality();
        }

        $this->decreaseSellIn();

        if ($this->isExpired()) {
            $this->setQuality(TypedItem::MIN_QUALITY);
        }
    }
}
