<?php

namespace App\Component\GildedRose\Interfaces;

/**
 * Interface TypedItemInterface
 * @package App\Component\GildedRose\Interfaces
 */
interface TypedItemInterface
{
    /**
     * @return mixed
     */
    public function update();
}
