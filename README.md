#Developer task for Telesoftas

##Stack
- [PHP 7.2](http://php.net/docs.php)
- [Docker](https://docs.docker.com/)
- [API-platform](https://api-platform.com)

##Structure
There are two parts of the task
1. Refactored GildedRose functionality
2. Created items REST API endpoints only index (items list) and show (item details)

##GildedRose component
There is a `GildedRose` component provided in API `api/src/Component/GildedRose`. You can view quality update results by
 running phpunit tests, where there is a test that return item information to console. Test running command is provided 
 below.

##Tests
```
docker-compose exec php bin/phpunit
```

###Confusion
I'm confused about task part:
```text
Implement GildedRose items rest service (only items list and item details endpoint) in one of these frameworks: API Platform (https://api-platform.com/), Symfony (https://symfony.com/). Use same code base for refactoring and rest service tasks.
```
I've created an endpoints which are for items list and item preview. But they do not contain any information as there 
are no endpoints for creation. Which variant I should approach:
- Create seed to fill database with data
- Also create create/update/delete endpoints
- Override default framework functionality to get data not from database but from some static dummy information
- Leave as it is, as this is enough

Also
- Should I combine GildedRose refactor task with API endpoint task? In that case Item class will be refactored which is 
not by GildedRose task requirements.


##Possibilities
- Create GildedRose entity and relate it with item entity
- Provide Behat tests for API endpoints. (Did not do that as this would be just a copy of API-platform tutorial for 
testing endpoints)
- More refactor on GildedRose task
- Implement more tests to cover GildedRose logic
- Combine GildedRose logic with API endpoints to have one gilded rose working solution
- Create a command for quality update

##Conclusion
GildedRose refactor task is really interesting as it could be achieved in different ways. I've had several versions how
to refactor it, and I think there are more of doing that.
Also it was interesting to look into a framework which I'm not familiar with. It really has lots of interested 
approaches to deal with stuff, which simplifies the process of boilerplate code.
